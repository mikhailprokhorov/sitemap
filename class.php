<?php
	use Bitrix\Main\Loader;

	if (! defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED != true) die;

	Loader::includeModule('iblock');

	class SiteMap extends CBitrixComponent
	{
		public function getResult()
		{
			$this->arResult['ITEMS'] = array_merge(
				$this->getMenus(),
				$this->getIBLocks()
			);
			if ($this->arParams['NEED_ELEMENTS']) {
				$this->arResult['ITEMS'] = array_merge(
					$this->arResult['ITEMS'],
					$this->getElements()
				);
			}
			return $this->arResult;
		}

		private function getElements()
		{
			$result = array();
			$iBlockIds = array();
			foreach ($this->getIBLocks() as $item) {
			    if (! in_array($item['IBLOCK_ID'], $iBlockIds)) {
			        $iBlockIds[] = $item['IBLOCK_ID'];
                    $obElements = CIBlocKElement::GetList(
                        array('SORT' => 'ASC'),
                        array(
                            'IBLOCK_ID' => $item['IBLOCK_ID'],
                            'ACTIVE' => 'Y'
                        ),
                        false,
                        false,
                        array('ID', 'IBLOCK_ID', 'NAME', 'DETAIL_PAGE_URL')
                    );
                    while ($element = $obElements->GetNext()) {
                        $element['LINK'] = $element['DETAIL_PAGE_URL'];
                        $element['DEPTH'] = (int) $item['DEPTH'] + 1;
                        $result[] = $element;
                    }
                }
            }
			return $result;
		}

		private function getIBLocks()
		{
			$result = array();
			foreach ($this->arParams['I_BLOCKS'] as $iBlockId) {
				$obSections = CIBlockSection::GetTreeList(
					array('IBLOCK_ID' => $iBlockId, 'ACTIVE' => 'Y', 'GLOBAL_ACTIVE' => 'Y'),
					array('ID', 'IBLOCK_ID', 'NAME', 'SECTION_PAGE_URL', 'DEPTH_LEVEL')
				);
				while ($section = $obSections->GetNext()) {
					$result[] = array(
						'NAME' => $section['NAME'],
						'LINK' => $section['SECTION_PAGE_URL'],
						'DEPTH' => $section['DEPTH_LEVEL'],
                        'IBLOCK_ID' => $section['IBLOCK_ID'],
                        'ID' => $section['ID'],
					);
				}
			}
			return $result;
		}

		private function getMenus()
		{
			global $APPLICATION;
			$result = array();
			foreach ($this->arParams['MENU_TYPES'] as $type) {
				foreach ($APPLICATION->GetMenu($type)->arMenu as $menuItem) {
					$result[] = array(
						'NAME' => $menuItem[0],
						'LINK' => $menuItem[1],
						'DEPTH' => 1,
					);
				}
			}
			return $result;
		}
	}
